package main

import (
	"fmt"
	"github.com/gocolly/colly"
	"gostudy/src/exercise/crawler/ex1/storage"
)

func main() {
	c := colly.NewCollector()

	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		e.Request.Visit(e.Attr("href"))
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	// replace default storage by FileStorage
	c.SetStorage(&storage.FileStorage{})

	c.Visit("http://go-colly.org/")
}
