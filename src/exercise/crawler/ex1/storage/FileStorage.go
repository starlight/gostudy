package storage

import (
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
)

// InMemoryStorage is the default storage backend of colly.
// InMemoryStorage keeps cookies and visited urls in memory
// and persists data on the disk.
type FileStorage struct {
	visitedURLS map[uint64]bool
	urlFile     *os.File // persist requestIds in urlFile
	cookieFile  *os.File // persist cookies in cookieFile
	lock        *sync.RWMutex
	jar         *cookiejar.Jar
}

// Init initializes FileStorage
func (f *FileStorage) Init() error {
	if f.visitedURLS == nil {
		f.visitedURLS = make(map[uint64]bool)
	}
	if f.urlFile == nil {
		var err error
		f.urlFile, err = os.OpenFile("crawlerUrlFile.dat", os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			return err
		}
	}
	if f.cookieFile == nil {
		var err error
		f.cookieFile, err = os.OpenFile("crawlerCookieFile.dat", os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			return err
		}
	}
	if f.lock == nil {
		f.lock = &sync.RWMutex{}
	}
	if f.jar == nil {
		var err error
		f.jar, err = cookiejar.New(nil)
		return err
	}
	return nil
}

// Visited implements Storage.Visited()
func (f *FileStorage) Visited(requestId uint64) error {
	f.lock.Lock()
	f.visitedURLS[requestId] = true
	_, err := f.urlFile.WriteString(strconv.FormatUint(requestId, 10) + "\n")
	if err != nil {
		log.Println(err)
	}
	f.lock.Unlock()
	return nil
}

func (f *FileStorage) IsVisited(requestId uint64) (bool, error) {
	f.lock.RLock()
	visited := f.visitedURLS[requestId]
	f.lock.RUnlock()
	return visited, nil
}

// Cookies implements Storage.Cookies()
func (f *FileStorage) Cookies(u *url.URL) string {
	return StringifyCookies(f.jar.Cookies(u))
}

// SetCookies implements Storage.SetCookies()
func (f *FileStorage) SetCookies(u *url.URL, cookies string) {
	f.jar.SetCookies(u, UnstringifyCookies(cookies))
}

// Close implements Storage.Close()
func (f *FileStorage) Close() error {
	if err := f.urlFile.Close(); err != nil {
		return err
	}
	return f.cookieFile.Close()
}

// StringifyCookies serializes list of http.Cookies to string
func StringifyCookies(cookies []*http.Cookie) string {
	cs := make([]string, len(cookies))
	for i, c := range cookies {
		cs[i] = c.String()
	}
	return strings.Join(cs, "\n")
}

// UnstringifyCookies deserializes a cookie string to http.Cookies
func UnstringifyCookies(s string) []*http.Cookie {
	h := http.Header{}
	for _, c := range strings.Split(s, "\n") {
		h.Add("Set-Cookie", c)
	}
	r := http.Response{Header: h}
	return r.Cookies()
}

// ContainsCookie checks if a cookie name is represented in cookies
func ContainsCookie(cookies []*http.Cookie, name string) bool {
	for _, c := range cookies {
		if c.Name == name {
			return true
		}
	}
	return false
}
